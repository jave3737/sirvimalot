# Cheatsheet

# Help 
```
:help quickfix
```
# Settings

```
set nohlsearch " removing highlighting from searches
set hlsearch! " toggle setting
```

# Fugitive

```
:G " show git window
:G <alias> " use alias in command
:GWrite <options> <path> " ADD files to staging, use git options
```
# Cheats

```
<c-q>{n}j{i}g<c-a> " highlight n lines vertically, increment by i
g<c-x> " decrement number vertically,
viwgU " visual inside word, capitalize all
viwgu " visual insdie word, lowercase all
gv " highlight previously highlighted area
<c-i> " go in jumplist
<c-o> " go out jumplist
:b# " switch to most recently used buffer
<c-^> " switch to most recently used buffer
<c-w>o " expand current buffer
:windo {command} " run the command on the current buffer, i.e :windo set nu, set number lines
:windo diffthis " compare buffers
:windo diffoff " stop comparing buffers
gt " go to next tab
gT " go to prev tab
{n}gt " go to nth tab
:tab[only] " close all tabs other than the current one
:tabdo {command} " run the command on all tabs, i.e :tabdo q, closes all windows
/{pattern} " search for pattern
?{pattern} " search for pattern in reverse
n " jump to next match
N " jump to prev match
* " jump to next match of word under cursor
# " jump to prev match of word under cursor
/\V{pattern} " very no magic, no special meaning characters
/\v{pattern} " very magic, use all available special meaning characters
/\c{pattern} " ignorecase
/\C{pattern} " case sensitive
:s:{pattern}:{replace} " replace pattern on first match
:s%:{pattern}:{replace} " replace pattern on first match, whole file
:s%:{pattern}:{replace}:g " replace pattern on all matches immediately, whole file
:s%:{pattern}:{replace}:gc " replace pattern on all matches with confirmations, whole file
:g:{pattern}: " find all pattern matches by line
:g:{pattern}:d " find all pattern matches by line, and delete
:g:{pattern}:s:{pattern}:{replace} " find all pattern matches by line, then replace pattern
:v:{pattern}: " find all pattern that don't match by line
qa " record to macro to register a
q " stop recording
@a " run macro stored in register a
@@ " rerun last macro
"ayy " store to register aa, yank contents
i<c-r>a " paste register a contents while in insert
:<c-r>a " paste register a contents to command
:cope[n] " open 
:ccl[ose] " close 
:cn[ext] " jump to next match
:cp[revious] " jump to prev match
:s:^://: " add // to start of line
vip:s:^://: " visual inside paragraph, add // to start of each line
```


